import pandas as pd
import numpy as np
import scipy.stats as stats
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
import glob
from PIL import Image
import sys
import os

def create_hidden_phases(real_phases):
    """Takes dictionary with real phases names as keys and returns list of dummy phases.
    For every phase with duration>1 dummy phases with according numbersuffix are created."""
    hidden_phases = []
    for phase in real_phases.keys():
        hidden_phases.append(phase)
        for i in range(2, real_phases[phase]["duration"] + 1):
            hidden_phases.append(phase + str(i))
    return hidden_phases


def calculate_next_gen(cur_gen, projects_p_a):
    """Takes pandas dataframe row and returns next year's row."""
    next_gen = cur_gen.copy()
    col_names = cur_gen.index

    for i in range(len(col_names) - 1, 0, -1):
        # updates column based on leftside value and successrate
        phase = col_names[i - 1]
        next_phase = col_names[i]
        project_count = cur_gen.loc[phase]
        real_phase = phases.get(next_phase, False)
        if real_phase:
            projects_added = stats.binom.rvs(n=project_count, p=real_phase["success"])
        else:
            projects_added = project_count

        if i == len(col_names)-1:
            next_gen.loc[next_phase] = projects_added + next_gen.loc[next_phase]
        else:
            next_gen.loc[next_phase] = projects_added

    next_gen.loc["target_find"] = projects_p_a

    return next_gen


def calculate_data(hidden_results, iterations, projects_p_a):
    """Takes number of iterations and projects per annum to start and a dataframe with hidden phases as column names and returns fully calculated hidden dataframe."""
    hidden_results = hidden_results.copy()
    hidden_results.loc[0]["target_find"] = hidden_results.loc[0]["target_find"] + projects_p_a

    for i in range(iterations):
        # add projects per annum to starting phase
        next_gen = calculate_next_gen(hidden_results.loc[i], projects_p_a)
        hidden_results.loc[i + 1] = next_gen
    return hidden_results


def collapse_data(hidden_results, real_phases, hidden_phases):
    """Takes the hidden_results and lists of phase names and returns real results with collapsed dummy phases added up."""
    results = pd.DataFrame(0, columns=real_phases, index=hidden_results.index)
    for real_phase in real_phases:
        for hidden_phase in hidden_phases:
            if real_phase in hidden_phase:
                results[real_phase] = results[real_phase] + hidden_results[hidden_phase]
    return results


def plot_row(row, year, max_val, save_to_file=True):
    tmp = plt.figure()
    row.plot(kind="bar", figsize=(7, 7))
    plt.ylabel("Projects")
    plt.xlabel("Phase")
    plt.ylim(0, max_val)
    plt.xticks([i for i in range(len(row.keys()))], row.keys(), rotation=45)
    plt.title("Year: {}".format(year))
    if save_to_file:
        plt.savefig("plots/year{}.png".format(year))
    else:
        plt.show()
    plt.close(tmp)


def create_bar_plots(results):
    # get the max value rounded up to next ten for y-axis
    max_val = results.max().max()
    max_val = max_val + 10 % max_val
    # pad indices with zeros so order of files are preserved for gif creation
    digits = len(str(len(results)))
    for index, row in results.iterrows():
        zeros_to_fill = digits - len(str(index))
        padded_index = "0" * zeros_to_fill + str(index)
        plot_row(row, padded_index, max_val)


def create_line_plot(results):
    fig, ax = plt.subplots(figsize=(15, 5))
    ax.set_title("Projects per phase over time")
    ax.set_xlabel("Year")
    ax.set_ylabel("Projects")
    fig.subplots_adjust(left=0.05, right=0.85, bottom=0.1, top=0.9)
    results.plot(kind="line", ax=ax)
    ax.legend(loc='center left', bbox_to_anchor=(1.0, 0.5))
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    ax.yaxis.set_major_locator(MaxNLocator(integer=True))
    plt.savefig("lineplot.png", bbox_inches="tight")


def make_gif(dir, gif_name):
    file_list = glob.glob('{}/*'.format(dir))
    frames = []
    for i in sorted(file_list):
        new_frame = Image.open(i)
        frames.append(new_frame)
    frames[0].save(gif_name, format='GIF',
                   append_images=frames[1:],
                   save_all=True,
                   duration=300, loop=0)


def run(phases, years, projects_p_a, create_files=True):
    hidden_phases = create_hidden_phases(phases)
    hidden_results = pd.DataFrame(0, columns=hidden_phases, index=range(1))
    finished_hidden_results = calculate_data(hidden_results, years, projects_p_a)
    results = collapse_data(finished_hidden_results, phases.keys(), hidden_phases)

    costs = calculate_cost(results, workers, cost_p_p)
    revenue = calculate_revenue(results, phases, patent_phase, patent_term, revenue_growth, revenue_decline,
                                max_revenue)
    profits = revenue - costs
    cum_profits = profits.cumsum()
    accounting_table = pd.concat([costs, revenue, profits, cum_profits], keys=["Costs", "Revenue", "Profit", "Cumulative Profits"], axis=1)
    investments = round(cum_profits.min()) * -1
    break_even = find_sign_changes(cum_profits)

    info_string = "Investments needed: {} million\nPayback year: {}\n{}\n{}".format(investments, break_even, accounting_table, results.to_string())
    print(info_string)

    if create_files:
        manual_string = "All parameters are defined at the bottom of the main.py. Plots will be overwritten when rerun with create_files=True. Uploaded plots are results of an example run. Associated prints follow here:\n\n"
        create_bar_plots(results)
        make_gif("plots", "mypharma.gif")
        create_line_plot(results)
        with open('results.html', 'w') as f:
            f.write(results.to_html(index=True))
        with open("info.txt", "w") as f:
            f.write(manual_string)
            f.write(info_string)
    return results, finished_hidden_results


def calculate_cost(results, workers, cost_p_p):
    """Takes results dataframe, workers per project per phase dictionary and cost per worker
    and returns costs per phase per year."""
    # copy results without the market phase since it has no costs/workers
    costs = results.copy().drop("market", axis=1)
    costs = costs.apply(lambda x: x.apply(
        lambda y: (workers[x.name]["base"] + workers[x.name]["per_project"] * y)
                  * cost_p_p)
                        )
    costs["total"] = costs.sum(axis=1)
    return costs["total"]


def get_release_age(phase, phases):
    """Takes a phase as string that has to be a key in a phases dictionary.
    Returns the total durations of all past phases including given phase."""
    phase_lst = list(phases.keys())
    relevant_phases = [p for i, p in enumerate(phase_lst) if i >= phase_lst.index(phase)]
    release_age = sum([phases[p]["duration"] for p in relevant_phases])
    return release_age


def extend_revenue_tables(phases, patent_phase, patent_term, revenue_growth, revenue_decline):
    """Calculates list of factors to multiply with max_revenue to get revenue. Index is year passed since marketphase
    starting at 0"""
    release_age = get_release_age(patent_phase, phases)
    on_market_patent_term = patent_term - release_age
    # decide if gain after release or loss of patent decay overlap. -1 because decay starts in year of patent loss
    range_of_revenue = max(on_market_patent_term + len(revenue_decline) - 1,
                           len(revenue_growth))
    extended_revenue_growth = revenue_growth + [revenue_growth[-1]] * (range_of_revenue - len(revenue_growth))
    extended_revenue_decline = [1.0] * (range_of_revenue - len(revenue_decline)) + revenue_decline
    return extended_revenue_growth, extended_revenue_decline


def projects_per_age_cat(results, age_categories):
    projects_by_age_p_a = pd.DataFrame(0, columns=range(age_categories), index=results.index)
    projects_by_age_p_a[0] = results["market"]

    tmp_row = projects_by_age_p_a.loc[0]
    for i, row in projects_by_age_p_a.iterrows():
        if i == 0:
            continue
        tmp_val = tmp_row.iloc[-1]
        tmp_row = tmp_row.shift(periods=1, fill_value=row.iloc[0])
        tmp_row.iloc[-1] = tmp_row.iloc[-1] + tmp_val
        projects_by_age_p_a.iloc[i] = tmp_row

    return projects_by_age_p_a


def calculate_revenue(results, phases, patent_phase, patent_term, revenue_growth, revenue_decline, max_revenue):
    extended_revenue_growth, extended_revenue_decline = extend_revenue_tables(phases, patent_phase, patent_term,
                                                                              revenue_growth, revenue_decline)
    age_categories = len(extended_revenue_growth)
    projects_per_age_p_a = projects_per_age_cat(results, age_categories)
    profits_per_age_p_a = projects_per_age_p_a.apply(
        lambda x: x * extended_revenue_growth[x.name] * extended_revenue_decline[x.name] * max_revenue)
    revenue_p_a = profits_per_age_p_a.sum(axis=1)

    return revenue_p_a


def find_sign_changes(cum_profits):
    """Finds first sign change."""
    signs = np.sign(cum_profits)
    diffs = np.diff(signs)
    sign_change_index = np.where(diffs != 0)[0] + 1
    if len(sign_change_index) == 0:
        return False
    else:
        return sign_change_index[0]


def calculate_payback(phases, years, projects_p_a,
                      workers, cost_p_p,
                      patent_phase, patent_term, revenue_growth, revenue_decline, max_revenue,
                      iterations=100, p=0.9):
    """Calculates the years need for being able to pay pack investor by a given chance p. Iterations is the amount of calculations done."""

    payback_year = [0 for i in range(years+1)]
    bankrupcies = 0
    investments = []

    for i in range(iterations):
        sys.stdout = open(os.devnull, 'w')
        results, finished_hidden_results = run(phases, years, projects_p_a, create_files=False)
        sys.stdout = sys.__stdout__
        costs = calculate_cost(results, workers, cost_p_p)
        revenue = calculate_revenue(results, phases, patent_phase, patent_term, revenue_growth, revenue_decline,
                                    max_revenue)
        profits = revenue - costs
        cum_profits = profits.cumsum()
        investments.append(cum_profits.min())
        break_even = find_sign_changes(cum_profits)

        if break_even:
            # if there was never a break even point, debt is never paid back
            payback_year[break_even] = payback_year[break_even] + 1
        else:
            bankrupcies = bankrupcies + 1

    cum_years = np.cumsum(payback_year)
    cum_chances = [x/iterations for x in cum_years]
    final_years = np.argwhere(np.array(cum_chances) >= p)
    if len(final_years) == 0:
        year = False
    else:
        year = final_years[0]
    return year, bankrupcies, round(np.mean(investments)) * -1


if __name__ == '__main__':
    # if False only one run is calculated, if True multiple runs are calculated for scenario prediction
    run_scenario = False
    # create plot files, if run_scenario = False
    create_files = False

    phases = {"target_find": {"duration": 2, "success": 0.9},
              "fst_tval": {"duration": 2, "success": 0.5},
              "hts": {"duration": 1, "success": 0.8},
              "lead_find": {"duration": 1, "success": 0.9},
              "scd_tval": {"duration": 1, "success": 0.5},
              "lead_opt": {"duration": 3, "success": 0.5},
              "profiling": {"duration": 1, "success": 0.33},
              "p_one": {"duration": 1, "success": 0.8},
              "p_two": {"duration": 2, "success": 0.5},
              "p_three": {"duration": 3, "success": 0.8},
              "submission": {"duration": 1, "success": 0.9},
              "market": {"duration": 0, "success": 1},
              }
    workers = {"target_find": {"base": 5, "per_project": 5},
               "fst_tval": {"base": 25, "per_project": 2},
               "hts": {"base": 27, "per_project": 3},
               "lead_find": {"base": 14, "per_project": 13},
               "scd_tval": {"base": 5, "per_project": 3},
               "lead_opt": {"base": 10, "per_project": 14},
               "profiling": {"base": 71, "per_project": 12},
               "p_one": {"base": 45, "per_project": 55},
               "p_two": {"base": 55, "per_project": 80},
               "p_three": {"base": 80, "per_project": 120},
               "submission": {"base": 5, "per_project": 8},
               }

    revenue_growth = [0.15, 0.3, 0.45, 0.6, 0.8, 1.0, 1.0]
    revenue_decline = [0.8, 0.5, 0.3, 0.2]
    patent_term = 20
    patent_phase = "profiling"

    projects_p_a = 10
    years = 50

    # cost per worker in millions
    cost_p_p = 0.125
    # max_revenue in millions
    max_revenue = 1000

    if run_scenario:
        iterations = 100
        p = 0.9

        # this calculates iterations amount of runs and returns the year in which chances of paying back investors is >= p
        payback_year, bankrupcies, investments = calculate_payback(phases, years, projects_p_a,workers, cost_p_p, patent_phase, patent_term, revenue_growth, revenue_decline, max_revenue, iterations, p)
        print("Years needed to pay back investors at {}% chance: {}\nCompanies without profit after {} years: {}\nMean investmentes needed: {} million".format(p*100, payback_year, years, bankrupcies, investments))
    else:
        # this makes one run
        results, finished_hidden_results = run(phases, years, projects_p_a, create_files=create_files)

